package main

import (
	"net/http"
	"os"
	"time"
)



func main() {
	port := os.Getenv("PORT")
	http.HandleFunc("/", URLHandler)
	go http.ListenAndServe(":"+port, nil)

	var hourCycle time.Duration

	for {
		delay := time.Minute * 10

		time.Sleep(delay)

		hourCycle = hourCycle + delay
		if hourCycle >= time.Minute * 1440 {
			db := setupDB()
			db.Init()
			db.HookEvaluation()

			DailyRoutine()

			hourCycle = time.Minute * 0
		}
	}
}