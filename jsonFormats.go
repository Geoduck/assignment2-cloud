package main

import "gopkg.in/mgo.v2/bson"

type CurrencyLoad struct {
	Id bson.ObjectId 		`bson:"_id,omitempty"`
	WebhookURL 		string 	`json:"webhookURL"`
	BaseCurrency 	string 	`json:"baseCurrency"`
	TargetCurrency 	string 	`json:"targetCurrency"`
	MinTriggerValue float64 `json:"minTriggerValue"`
	MaxTriggerValue float64 `json:"maxTriggerValue"`
}

/*	eks payload for currency load
{
"webhookURL":"keke",
"baseCurrency":"NOK",
"targetCurrency":"EUR",
"minTriggerValue":2.3,
"maxTriggerValue":5.6
}

{
	"baseCurrency" :
	"targetCurrency"
	"currentRare"
	"minTriggerValue"
	"maxTriggerValue"
}
"
*/

type HookLoad struct {
	BaseCurrency 	string 	`json:"baseCurrency"`
	TargetCurrency 	string 	`json:"targetCurrency"`
	CurrentRate		float64 `json:"currentRare"`
	MinTriggerValue float64 `json:"minTriggerValue"`
	MaxTriggerValue float64 `json:"maxTriggerValue"`

}


type RawFixer struct {
	Base 	string 	 			 `json:"base"`
	Date 	string 	 			 `json:"date"`
	LocalRate map[string]float64 `json:"rates"`
}


type Exchange struct {
	Valuta map[string]interface{} `json:"-"`
}

type Rates struct {
	BaseCurrency 	string
	TargetCurrency 	string
}