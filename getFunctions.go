package main

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"fmt"
	"log"
)


func GetBody(url string, myClient *http.Client) ([]byte, error) {

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		body := []byte(nil)
		return body, err
	}

	// Apply header to request
	req.Header.Set("User-Agent", "Mathias")

	// Try to execute request
	res, doError := myClient.Do(req)
	if doError != nil {
		body := []byte(nil)
		return body, doError
	}

	body, readError := ioutil.ReadAll(res.Body)
	if readError != nil {
		rtrError := readError
		return body, rtrError
	}

	defer res.Body.Close()

	return body, nil
}

func getCurrencyLoad (r *http.Request) (CurrencyLoad, error){

	currencyLoad := CurrencyLoad{}

	err := json.NewDecoder(r.Body).Decode(&currencyLoad)
	if err != nil {
		log.Printf("Failed to decode data! Error: %v", err)
	}
	defer r.Body.Close()

	return currencyLoad, err

}


func getRates (myClient *http.Client, base string) (interface{}, error) {
	var jsonFixer interface{}

	fixerURL := "http://api.fixer.io/latest?base=" + base

	fixerBody, error := GetBody(fixerURL, myClient)
	if error != nil {
		log.Print("could not retrive fixer data", error.Error())
		return jsonFixer, error
	}
	err := json.Unmarshal(fixerBody, &jsonFixer)
	if err != nil {
		log.Print("Failed to convert data", error.Error())
		return jsonFixer, err
	}
	return jsonFixer, err
}


func GetHookContent(hook CurrencyLoad, rawFixer RawFixer) string{
	a := fmt.Sprint(rawFixer.LocalRate[hook.TargetCurrency])
	b := fmt.Sprint(hook.MaxTriggerValue)
	c := fmt.Sprint(hook.MinTriggerValue)

	hookContent := "baseCurrency: " + hook.BaseCurrency + "\ntargetCurrency: " + hook.TargetCurrency +
		"\ncurrentRate: " + a + "\nmaxTriggerValue" +
		b + "\nminTriggerValue" + c

	return hookContent
}


// GetHTTP403 ...
// Redirect user to errorpage
func GetHTTP403(w http.ResponseWriter, failed string) {
	w.WriteHeader(http.StatusForbidden)
	http.Error(w, "Could not get "+failed+" data", 403)

}

