package main
/*
sources:
https://github.com/marni/imt2681_cloud/blob/master/mongodb/database.go
 */

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"fmt"
	"net/http"
	"time"
	"log"
	"encoding/json"
)

type MongoDBInfo struct {
	MongoURL string
	DatabaseName string
	CurrencyHookCollection string
	RatesCollection string
}


func setupDB() *MongoDBInfo {
	db := MongoDBInfo{"mongodb://Geoduck:password@ds145275.mlab.com:45275/currencydb",
	"currencydb",
	"HookCollection",
	"RatesCollection"}

	_, err := mgo.Dial(db.MongoURL)
	if err != nil{
		log.Print("Failed to obtain session")
	}

	return &db
}



func (db *MongoDBInfo) Init() {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}

	defer session.Close()

	index := mgo.Index{
		Key:		[]string{"id"},
		Unique:     true,
		DropDups:   false,
		Background: true,
		Sparse: 	true,

	}

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).EnsureIndex(index)
	if err != nil {
		panic(err)
	}
}

func (db *MongoDBInfo) Add(c CurrencyLoad) error {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		log.Printf("Error adding %v", err.Error())
	}

	defer session.Close()


	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Insert(c)
	if err != nil {
		log.Printf("Error inserting new hook %v", err.Error())
		return err
	}

	return nil
}


func (db *MongoDBInfo) Count() int {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// handle to "db"
	count, err := session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Count()
	if err != nil {
		log.Printf("error in Count(): %v", err.Error())
		return -1
	}

	return count
}


func (db *MongoDBInfo) Get(HookURL string) (CurrencyLoad, bool) {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}

	defer session.Close()

	 currencyData := CurrencyLoad{}
	 gotData := true

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(bson.M{"webhookurl":HookURL}).One(&currencyData)
	if err != nil {
		gotData = false
		return currencyData, gotData
	}

	return currencyData, gotData
}

func (db *MongoDBInfo) Exist(HookURL string) int {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}
	defer session.Close()

	count, erro := session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(bson.M{"webhookurl":HookURL}).Count()
	if erro != nil {
		fmt.Printf("Error in count %v", erro.Error())
		count = -1
	}
	return count
}

func (db *MongoDBInfo) GetByID(id string) (CurrencyLoad, bool) {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}

	defer session.Close()

	currencyData := CurrencyLoad{}
	gotData := true

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(bson.M{"_id":bson.ObjectIdHex(id)}).One(&currencyData)
	if err != nil {
		gotData = false
	}

	return currencyData, gotData
}

func (db *MongoDBInfo) Delete(id string) bool {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}

	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Remove(bson.M{"_id":bson.ObjectIdHex(id)})
	if err != nil {
		return false
	}

	return  true
}

func (db *MongoDBInfo) DailyFix() {

	myClient := http.Client{
		Timeout: time.Second * 2,
	}

	rates, err := getRates(&myClient, "EUR")
	if err != nil {
		log.Printf("Could not get daily fix! \nError: %v", err)
		println("jh;kjlh")
	}

	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}

	defer session.Close()


	err = session.DB(db.DatabaseName).C(db.RatesCollection).Insert(rates)
	if err != nil {
		log.Printf("Could not get daily fix! \nError: %v", err)
	}

}

func (db *MongoDBInfo) GetLocalFixer(date string) (interface{}, bool) {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}

	defer session.Close()

	var ratesData interface{}
	gotData := true

	err = session.DB(db.DatabaseName).C(db.RatesCollection).Find(bson.M{"date":date}).One(&ratesData)
	if err != nil {
		gotData = false
	}

	return ratesData, gotData
}

func localFixerFunction (s *mgo.Session, ) {


}


func (db *MongoDBInfo) RetrieveAllHooks() {

	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}

	defer session.Close()

	aDate := time.Now().Local()
	stringTodayDate := aDate.Format("2006-01-02")

	var fixerInterface interface{}
	rawFixer := RawFixer{}

	err = session.DB(db.DatabaseName).C(db.RatesCollection).Find(bson.M{"date":stringTodayDate}).One(&fixerInterface)
	if err != nil {
		log.Printf("Could not find date! Error: %v", err.Error())
		return
	}

	dayRateBody, err := json.Marshal(fixerInterface)
	if err != nil {
		log.Printf("Failed to convert data! Error: %v", err)
		return
	}

	err = json.Unmarshal(dayRateBody, &rawFixer)
	if err != nil {
		log.Printf("Failed to convert data! Error: %v", err)
		return
	}

	hook := CurrencyLoad{}
	dbSize, err := session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Count()
	if err != nil {
		log.Printf("Failed to count db! Error: %v", err)
		return
	}

	if dbSize > 0 {
		for i := 1; i <= dbSize; i++ {
			err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(nil).Skip(dbSize-1).One(&hook)
			if err != nil {
				log.Printf("Failed to retrieve data! Error: %v", err)
				return
			}


			if hook.MaxTriggerValue < rawFixer.LocalRate[hook.TargetCurrency] ||
				hook.MinTriggerValue > rawFixer.LocalRate[hook.TargetCurrency] {

				hookContent := GetHookContent(hook, rawFixer)
				WebHookEntry(hookContent, hook.WebhookURL)

			}
		}
	}


}



func (db *MongoDBInfo) HookEvaluation() {

	session, err := mgo.Dial(db.MongoURL)
	if err != nil {
		panic(err)
	}

	defer session.Close()

	aDate := time.Now().Local()
	stringTodayDate := aDate.Format("2006-01-02")

	var fixerInterface interface{}
	rawFixer := RawFixer{}

	err = session.DB(db.DatabaseName).C(db.RatesCollection).Find(bson.M{"date": stringTodayDate}).One(&fixerInterface)
	if err != nil {
		log.Printf("Could not find date! Error: %v", err.Error())
		return
	}

	dayRateBody, err := json.Marshal(fixerInterface)
	if err != nil {
		log.Printf("Failed to convert data! Error: %v", err)
		return
	}

	err = json.Unmarshal(dayRateBody, &rawFixer)
	if err != nil {
		log.Printf("Failed to convert data! Error: %v", err)
		return
	}

	hook := CurrencyLoad{}
	dbSize, err := session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Count()
	if err != nil {
		log.Printf("Failed to count db! Error: %v", err)
		return
	}

	if dbSize > 0 {
		for i := 1; i <= dbSize; i++ {
			err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Find(nil).Skip(dbSize - 1).One(&hook)
			if err != nil {
				log.Printf("Failed to retrieve data! Error: %v", err)
				return
			}

			println(hook.TargetCurrency)
			hookContent := GetHookContent(hook, rawFixer)
			WebHookEntry(hookContent, hook.WebhookURL)

		}
	}

}

func (db *MongoDBInfo) DeleteByURL(url string) bool {
	session, err := mgo.Dial(db.MongoURL)
	if err != nil{
		panic(err)
	}

	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CurrencyHookCollection).Remove(bson.M{"webhookurl":url})
	if err != nil {
		return false
	}

	return  true
}