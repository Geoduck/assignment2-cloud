package main

//todo: replace print with errorhandling

import (
	"net/http"
	"encoding/json"
	"strings"
	"fmt"
	"time"
	"regexp"
)

func RootHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		myClient := http.Client{
			Timeout: time.Second * 2,
		}

		r.Header.Set("Content-Type", "application/json")

//		rootURL := r.URL.Path

		newCurrencyLoad, err := getCurrencyLoad(r)
		if err != nil {
			http.Error(w, "Could not get Currencyload", http.StatusInternalServerError)
			return
		}

		if newCurrencyLoad.BaseCurrency != "EUR" {
			http.Error(w, "This base currency is not implemented yet", http.StatusNotImplemented)
			return
		}

		validString, err := regexp.MatchString("^[A-Z]{3}$", newCurrencyLoad.TargetCurrency)
		if err != nil {
			http.Error(w, "Invalid target currency string", http.StatusBadRequest)
			return
		} else if validString == false {
			println("Invalid target string")
			return
		}

		db := setupDB()
		db.Init()

		// 2 check if valuta is valid form api
		// Create a fixer url
		fixerURL := "http://api.fixer.io/latest?base=EUR"

		// Retrieve json from url
//		var rawFixer interface{}

		fixerBody, fixerError := GetBody(fixerURL, &myClient)
		if fixerError != nil {
			println("could not retrive fixer data")
			return
		}

		fixerString := string(fixerBody[:])

		if !strings.Contains(fixerString, newCurrencyLoad.TargetCurrency){
			println("Target currency is not valid")
		}

		// 3 save webhook to mongo db
		newCurrencyLoad.BaseCurrency = "EUR"

		err = db.Add(newCurrencyLoad)
		if err != nil {
			http.Error(w,"Failed to add", http.StatusInternalServerError )
			return
		}

		addedHook, found := db.Get(newCurrencyLoad.WebhookURL)
		if found == false {
			http.Error(w, "Error retrieving ID", http.StatusInternalServerError)
			return
		}

		frontID := strings.Split(addedHook.Id.String(), "\"")

		fmt.Fprintf(w, "ID: %s",  frontID[1])


		// 4 give feedback success



	} else {
		http.Error(w, "Please post json to the webhook", http.StatusBadRequest)
	}

}

func HookHandler(w http.ResponseWriter, r *http.Request) {
	hookUrl := r.URL.Path

	db := setupDB()

	currency, found := db.Get(hookUrl)
	if found == false {
		http.Error(w, "Could not find hook", http.StatusInternalServerError)
		return
	}

	json.Marshal(&currency)
	json.NewEncoder(w).Encode(currency)

}

func IdHandler(w http.ResponseWriter, r *http.Request) {

	db := setupDB()
	db.Init()


	hookUrl := r.URL.Path

	IDString := strings.Split(hookUrl, "/")
	if r.Method == "GET" {

		webhookID, got := db.GetByID(IDString[1])
		if got == false {
			http.Error(w, "Could not find webhook", http.StatusBadRequest)
			return
		}

		http.Header.Add(w.Header(), "content-type", "application/json")
		json.Marshal(&webhookID)
		json.NewEncoder(w).Encode(webhookID)

	} else if r.Method == "DELETE" {
		delete := db.Delete(IDString[1])
		if delete == false {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
}

func LatestHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST"{
		db := setupDB()
		db.Init()

		postRates := Rates{}

		err := json.NewDecoder(r.Body).Decode(&postRates)
		if err != nil {
			http.Error(w, "Could not retrive data from post", http.StatusConflict)
			return
		}

		//Get current date
		aDate := time.Now().Local()
		stringTodayDate := aDate.Format("2006-01-02")

		rawFixer := RawFixer{}

		DayRate, gotLocal := db.GetLocalFixer(stringTodayDate)
		if gotLocal == false {
			aDate = aDate.AddDate(0, 0, -1)
			stringTodayDate = aDate.Format("2006-01-02")

			DayRate, gotLocal = db.GetLocalFixer(stringTodayDate)
			if gotLocal == false {
				http.Error(w, "missing data about today", http.StatusInternalServerError)
				return
			}
		}

		dayRateBody, err := json.Marshal(DayRate)
		if err != nil {
			http.Error(w, "Failed to convert internal data", http.StatusInternalServerError)
			return
		}

		err = json.Unmarshal(dayRateBody, &rawFixer)
		if err != nil {
			http.Error(w, "Failed to convert internal data", http.StatusInternalServerError)
			return
		}

		fmt.Fprintln(w, rawFixer.LocalRate[postRates.TargetCurrency])

	} else  {
		http.Error(w, "Nothing to GET", http.StatusBadRequest)
	}

}

func AverageHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {

		const AverageOf = 3

		postRates := Rates{}

		err := json.NewDecoder(r.Body).Decode(&postRates)
		if err != nil {
			http.Error(w, "Could not retrive data from post", http.StatusConflict)
			return
		}

		db := setupDB()
		db.Init()


		//Get current date
		aDate := time.Now().Local()
		stringTodayDate := aDate.Format("2006-01-02")

		rawFixer := RawFixer{}

		var rateSum float64

		for i := 0; i < AverageOf; i++ {

			aDate = time.Now().Local().AddDate(0, 0, -i)
			stringTodayDate = aDate.Format("2006-01-02")

			println(stringTodayDate)

			DayRate, gotLocal := db.GetLocalFixer(stringTodayDate)
			if gotLocal == false {
				http.Error(w, "Missing data about one date", http.StatusInternalServerError)
				return
			}

			dayRateBody, err := json.Marshal(DayRate)
			if err != nil {
				http.Error(w, "Failed to convert internal data", http.StatusInternalServerError)
				return
			}

			err = json.Unmarshal(dayRateBody, &rawFixer)
			if err != nil {
				http.Error(w, "Failed to convert internal data", http.StatusInternalServerError)
				return
			}


			rateSum += rawFixer.LocalRate[postRates.TargetCurrency]
		}

		averageRate := rateSum/AverageOf

		fmt.Fprintln(w, averageRate)

	} else {
		http.Error(w, "Nothing to GET", http.StatusBadRequest)
	}

}

func EvalHandler(w http.ResponseWriter, r *http.Request) {
	db := setupDB()
	db.Init()
	db.HookEvaluation()
}

func URLHandler(w http.ResponseWriter, r *http.Request) {

	URLCheck := r.URL.Path

	URLSplit := strings.Split(URLCheck, "/")

	if  strings.ToLower(URLSplit[1]) == "" {
		RootHandler(w, r)
	} else if strings.ToLower(URLSplit[1]) == "latest" {
		LatestHandler(w, r)
	} else if strings.ToLower(URLSplit[1]) == "average" {
		AverageHandler(w, r)
	} else if strings.ToLower(URLSplit[1]) == "evaluationtrigger" {
		EvalHandler(w, r)
	} else {
		IdHandler(w, r)
	}
}

