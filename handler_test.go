package main

// source https://elithrar.github.io/article/testing-http-handlers-go/

import (
	"testing"
	"net/http"
	"net/http/httptest"
	"bytes"
	"strings"
)

func TestRootHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(RootHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	testBody := []byte(`{"webhookURL":"delete", "baseCurrency":"EUR", "targetCurrency":"NOK", "minTriggerValue":2.3, "maxTriggerValue":5.6}`)
	testIo := bytes.NewReader(testBody)

	req, err = http.NewRequest("POST", "/", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(RootHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if !strings.Contains(rr.Body.String(), "ID") {
		t.Error("Request body is not corerct, : got %s want %s", rr.Body.String(), "ID")
	}

	db := setupDB()
	db.Init()
	db.DeleteByURL("delete")
}

func TestIdHandler(t *testing.T) {
	req, err := http.NewRequest("POST", "/59faff96d6deb8574f10ddab", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(IdHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}


	req, err = http.NewRequest("GET", "/59faff96d6deb8574f10ddab", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(IdHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if !strings.Contains(rr.Body.String(), `"baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":1.5,"maxTriggerValue":2.55`) {
		t.Error("Request body is not corerct, : got %s want %s", rr.Body.String(), `"baseCurrency":"EUR","targetCurrency":"NOK","minTriggerValue":1.5,"maxTriggerValue":2.55`)
	}


	/*req, err = http.NewRequest("DELETE", "/59fa46bcd6deb8574f0f241e", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(IdHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}*/


}

func TestLatestHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/latest", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(LatestHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}


	testBody := []byte(`{"baseCurrency":"EUR", "targetCurrency":"NOK"}`)
	testIo := bytes.NewReader(testBody)


	req, err = http.NewRequest("POST", "/latest", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(LatestHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}


func TestAverageHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/average", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AverageHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}


	testBody := []byte(`{"baseCurrency":"EUR", "targetCurrency":"NOK"}`)
	testIo := bytes.NewReader(testBody)


	req, err = http.NewRequest("POST", "/average", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(AverageHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK{
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	/*rBody := strings.TrimSpace(rr.Body.String())

	bodyNumber, err := strconv.ParseFloat(rBody, 64)
	if err != nil {
		t.Error("Failed to convert body to float")
	}

	if bodyNumber > 15 || bodyNumber < 4 {
		t.Error("Probably wrong calculations. Average is: ", rr.Body.String())
	}*/

}

func TestURLHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}
	

	req, err = http.NewRequest("GET", "/average", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}

	testBody := []byte(`{"baseCurrency":"EUR", "targetCurrency":"NOK"}`)
	testIo := bytes.NewReader(testBody)

	req, err = http.NewRequest("POST", "/average", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)

	}

	req, err = http.NewRequest("GET", "/latest", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusBadRequest)
	}


	testBody = []byte(`{"baseCurrency":"EUR", "targetCurrency":"NOK"}`)
	testIo = bytes.NewReader(testBody)


	req, err = http.NewRequest("POST", "/latest", testIo)
	if err != nil {
		t.Fatal(err)
	}

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(URLHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Error("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}