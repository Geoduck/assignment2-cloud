package main

import (
	"encoding/json"
	"net/http"
	"bytes"
	"fmt"
	"io/ioutil"
	"time"
)

type WebhookInfo struct {
	Content string `json:"content"`
}

func DailyRoutine() {
	db := setupDB()
	db.Init()
	db.DailyFix()
	db.RetrieveAllHooks()
}

func WebHookEntry(what string, URL string) {
	info := WebhookInfo{}
	info.Content = what + "\n"
	raw, _ := json.Marshal(info)
	resp, err := http.Post(URL, "application/json", bytes.NewBuffer(raw))
	if err != nil {
		fmt.Println(err)
		fmt.Println(ioutil.ReadAll(resp.Body))
	}
}


func hookmain() {
	for {
		//text := "Heroku timer test at: " + time.Now().String()
		delay := time.Minute * 15

		DailyRoutine()
		time.Sleep(delay)
	}
}
